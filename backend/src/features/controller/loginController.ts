import { Request, Response } from 'express';
import LoginModel from '../model/loginModel';

const loginModel = new LoginModel();

const jwt = require('jsonwebtoken');
const SECRET = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'

export default {
  async Auth(request: Request, response: Response) {
    try {
      const { name, password } = request.body.data

      const [login] = await loginModel.authName({ name })

      if (password == login.password) {
        const token = jwt.sign({ userId: login.id }, SECRET, { expiresIn: 3000 })

        response.status(201).json({ token: token, result: login, mensagem: 'Usuário autenticado' })
      } else {
        response.status(400).json({ mensagem: 'Falha na autenticação' })
      }
    }
    catch (err) {
      response.status(500).send({ erro: err })
    }
  },
}
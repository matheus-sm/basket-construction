import { Client } from 'pg';

export default class Connection {

  private static instance: Connection

  public static getInstance(): Connection {
    if (!Connection.instance) {
      Connection.instance = new Connection()
    }
    return Connection.instance
  }

  public executeQuery(sql: string, values?: Array<any>): Promise<any> {

    const client = new Client({
      user: 'postgres',
      host: 'localhost',
      database: 'bascket_construction',
      password: 'postgres',
      port: 5432
    })

    return new Promise(async (resolve, reject) => {
      try {
        await client.connect()
        const response = await client.query(sql, values)
        client.end()
        resolve(response)
      } catch (error) {
        reject(error)
      }
    })
  }
}
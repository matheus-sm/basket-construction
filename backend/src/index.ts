import routes from './_config/routes';
import cors from 'cors';
import express from 'express'

try{
    const app = express()
    app.use(cors())
    app.use(express.json())
    app.use(routes)
    app.listen(3001)
    console.log("App rodando")
}
catch{
    console.log("Erro ao rodar o App");
}

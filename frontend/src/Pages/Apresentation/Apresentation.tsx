import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import { AiFillDatabase } from 'react-icons/ai';
import { BsTwitter, BsFacebook, BsInstagram } from 'react-icons/bs';
import { RiFocus2Fill } from 'react-icons/ri';
import { GiInspiration } from 'react-icons/gi';

import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';

import Donation from '../../Images/donation.svg';

import "./Styles/Apresentation.css"

function WithHeaderExample() {
  return (
    <Card className="text-center">
      <Card.Header>
        <Navbar>
          <Container>
            <Navbar.Brand>Gestão Pública</Navbar.Brand>
            <Navbar.Collapse className="justify-content-end">
              <Button href="/login" variant="success">Realizar Login</Button>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </Card.Header>

      <Card.Body>
        <div className="pt-5 pb-5">
          <Container >
            <Row>
              <Col lg={6}>
                <img src={Donation} alt="business" width="100%" />
              </Col>
              <Col lg={6} className="salutation text-center align-self-center">
                <h2 className="text-uppercase"> Bem vindo </h2>
                <p className="mb-4 mt-4">
                  Expicação do site e caso não tiver conta ainda clique no botão a baixo.
                </p>
                <Button href="/userRegistration" className="btn-outline --grey">Criar Conta</Button>
              </Col>
            </Row>
            <section>
              <Container>
                <Row className="mt-5 text-center justify-content-center">
                  <Col lg={4}>
                    <div className="pane">
                      <div className="d-flex justify-content-center">
                        <i><RiFocus2Fill/></i>
                      </div>
                      <h3>Objetivos</h3>
                      <p> Salientar a fome de todos, sem classificação.</p>
                    </div>
                  </Col>
                  <Col lg={4}>
                    <div className="pane">
                      <div className="d-flex justify-content-center">
                        <i className="ml-2" ><AiFillDatabase /></i>
                      </div>
                      <h3>Resultados</h3>
                      <p> Alienação da fome por Marilía.</p>
                    </div>
                  </Col>
                  <Col lg={4}>
                    <div className="pane">
                      <div className="d-flex justify-content-center">
                        <i><GiInspiration /></i>
                      </div>
                      <h3>
                        Motivação
                      </h3>
                      <p> Nos motivamos com a felicidade de cada usuário com suas requisições atendidas.</p>
                    </div>
                  </Col>
                </Row>
              </Container>
            </section>
          </Container>
        </div>
      </Card.Body>

      <Card.Footer className="footer text-muted">
        <Row>
          <Col lg={6} className="text-lg-left text-sm-center">
            <h2 className="font-weight-bold">Prefeitura Municipal de Marília</h2>
            <p> Rua: Bahia, nº 40 - Centro - Marília/SP</p>
            <p>Telefone: (14) 3413-1854</p>
          </Col>
          <Col lg={6} className="align-self-center">
            <div className="leads text-right text-sm-center" >
              <a className="m-3" href="https://www.facebook.com" target="_blank">
                <i><BsFacebook/></i>
              </a>
              <a className="m-2" href="https://www.twitter.com" target="_blank">
                <i><BsTwitter /></i>
              </a>
              <a className="m-2" href="http://www.instagram.com" target="_blank">
                <i><BsInstagram /></i>
              </a>
            </div>
          </Col>
          <div className="text-center">
            <Col lg={12} >
              <p>Copyright Município de Marília &copy;| Todos direitos reservados</p>
            </Col>
          </div>
        </Row>
      </Card.Footer>
    </Card>
  );
}

export default WithHeaderExample;
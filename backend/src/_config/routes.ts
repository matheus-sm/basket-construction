import { Router } from "express";
const routes = Router();

import loginController from '../features/controller/loginController';
import userController from '../features/controller/userController';
import requestController from '../features/controller/requestController';
import entityController from '../features/controller/entityController';
import verifyJWT from '../middlewares/authMideware'

export default routes;

//login
routes.post('/api/login', loginController.Auth)

//post User 
routes.post('/api/user/insertUser', userController.insertUser)

//Users List
routes.get('/api/user/listAllUser', verifyJWT.verifyJWT, userController.listAllUser)

//Requisition List
routes.get('/api/request/listAllRequests', verifyJWT.verifyJWT, requestController.listAllRequests)

//Requisition List By Id
routes.get('/api/request/listById/:id_entity', verifyJWT.verifyJWT, requestController.listById)

//Requisition List By Id
routes.get('/api/request/listOneRequest/:id', verifyJWT.verifyJWT, requestController.listOneRequest)

//Post Entity 
routes.post('/api/entity/',  verifyJWT.verifyJWT, entityController.insertEntity)


//Post Entity 
routes.get('/api/entity/:id',  verifyJWT.verifyJWT, entityController.listOneEntity)

//Post Requests 
routes.post('/api/request/', verifyJWT.verifyJWT, requestController.insertRequests)

//Put Requests 
routes.put('/api/request/:id', verifyJWT.verifyJWT, requestController.updateRequests)

//Delete Requests 
routes.delete('/api/request/:id', verifyJWT.verifyJWT, requestController.deleteRequests)
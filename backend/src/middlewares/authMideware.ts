import { NextFunction } from "express";
import { Request, Response } from 'express';
import LoginModel from '../features/model/loginModel';

const loginModel = new LoginModel();

const jwt = require('jsonwebtoken');

const SECRET = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'

export default {
  async verifyJWT(request: Request, response: Response, next: NextFunction) {
    const {
      name,
      password
    } = request.body

    try {
      const [login] = await loginModel.authName({ name })

      const token = request.headers['x-access-token']

      jwt.verify(token, SECRET, (err: any, decoded: { userId: Record<string, any> | undefined; }) => {
        if (err)
          return response.status(401).json({ mensagem: 'Usuário não autenticado' }).end()

        next()
      })
    }
    catch (err) {
      response.status(500).send({ erro: err })
    }
  },
}
import api from '../../Services/api'

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import { AxiosError } from 'axios'
import { useState } from 'react'

import { AiOutlineInfoCircle } from 'react-icons/ai';
import Alert from 'react-bootstrap/Alert';

import "./Styles/Entity.css";

function EntityRegistration() {
  // const [id_user, setId_user] = useState()
  const [name, setName] = useState('')
  const [nickname, setNickname] = useState('')
  const [address, setAddress] = useState('')
  const [email, setEmail] = useState('')
  const [city, setCity] = useState('')

  function exit() {
    window.localStorage.removeItem('entityid')
    window.localStorage.removeItem('token')
    window.localStorage.removeItem('userid')

    window.location.href = '/login'
  }
  async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    try {
      event.preventDefault()

      const userid = localStorage.getItem('userid')

      const data = {
        id_user: userid,
        name: name,
        nickname: nickname,
        address: address,
        email: email,
        city: city
      }

      await api.post('api/entity', { data })

      exit()
    } catch (ex: unknown) {
      if (ex instanceof AxiosError) {
        alert(`ERRO: ${ex?.response?.data.mensagem}`)
      }
    }
  }
  return (
    <section className="content">
      <div className="box">
        <div className="box-body nav-tabs-custom">
          <Row>
            <Col sm={12} md={12} xs={12}>
              <Alert className=" bg-info text-info">
                <AiOutlineInfoCircle />
                <strong > OBSERVAÇÃO:</strong> Para se cadastrar preencha as informações abaixo e clique em
                salvar, os campos com asterisco ( * ) são obrigatórios.
              </Alert>
            </Col>
          </Row>
          <Form data-toggle="validator" onSubmit={handleSubmit}>
            <Row>
              <Col sm={6} md={6} xs={6}>
                <Form.Group className="form-group">
                  <Form.Label>Nome *</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Nome" onChange={event => setName(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={6} md={6} xs={6}>
                <Form.Group className="form-group">
                  <Form.Label>Apelido *</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Apelido" onChange={event => setNickname(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={4} md={4} xs={4}>
                <Form.Group className="form-group">
                  <Form.Label>Endereço *</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Endereço" onChange={event => setAddress(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={4} md={4} xs={4}>
                <Form.Group className="form-group">
                  <Form.Label>E-mail *</Form.Label>
                  <Form.Control className="border" type="email" placeholder="E-mail" onChange={event => setEmail(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={4} md={4} xs={4}>
                <Form.Group className="form-group">
                  <Form.Label>Cidade *</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Cidade" onChange={event => setCity(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={12} md={12} xs={12} className="text-right">
                <Button type="submit" className="mt-3 btn btn-primary btn-cadastrar text-right" >
                  Cadastrar
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    </section>
  );
}

export default EntityRegistration;
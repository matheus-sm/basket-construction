import Connection from "../../database/connection"
const connection: Connection = Connection.getInstance()

interface IEntity {
  id?: any,
  id_user?: string,
  name?: string,
  nickname?: string,
  address?: string,
  email?: string,
  city?: string
}
export default class EntityModel {

  async insertEntity(entity: IEntity) {
    try {
      const sql = `
      insert into public.entity (
        id_user,
        name,
        nickname,
        address,
        email,
        city
      )
      values (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6
      )`

      const { id_user, name, nickname, address, email, city } = entity

      const values = [id_user, name, nickname, address, email, city]

      const res = await connection.executeQuery(sql, values)

      return res.rows;
    } catch (ex) {
      throw ex
    }
  }

  public async getOneEntity(request: IEntity): Promise<IEntity> {
    try {
      const sql = `select name, address, email, city from public.entity where id = $1`

      const { id } = request

      const values = [id]

      const res = await Connection.getInstance().executeQuery(sql, values);

      return res.rows;
    } catch (ex) {
      throw ex
    }
  }
}
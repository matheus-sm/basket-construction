
# Basket Construction

O projeto Basket Construction tem o seu back-end feito em Node.js, usando o banco
de dados Postgres e sua principal linguagem é TypeScript
## Instalação

Instale Basket Construction com npm

```bash
   npm install

```

```bash
   npm start
```
    
## Anotações sobre o banco de dados

Rodar os Scrpits do bloco de notas Banco Postgres.

Para funcionamento correto é necessário o banco de dados Postgres
 e realizar a configuração no projeto sendo usuário, host, database, senha e porta em
 src->database->connection.ts.



## Documentação da API

Para fins de teste e desenvolvimento foi feita a Documentação
 através do postman e está disponivel no link: 
https://documenter.getpostman.com/view/21069901/2s84LGWaTS

## Autores

- [@Matheus Sommacal](https://gitlab.com/matheus-sm)


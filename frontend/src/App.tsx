import React from 'react';
import Home from './Pages/Home/Home';
import Login from './Pages/Login/Login';
import Request from './Pages/Requests/Request';
import RequestById from './Pages/Requests/RequestById';
import UserRegistration from './Pages/UserRegistration/User';
import EntityRegistration from './Pages/EntityRegistration/Entity';
import RequestsRegistration from './Pages/RequestsRegistration/Request';
import Apresentation from './Pages/Apresentation/Apresentation';

import './App.css';

import NavBar from './Components/NavBar/Index';

import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import { useState } from 'react'

function App() {
  const [isAuth, setIsAuth] = useState(!!window.localStorage.getItem('token'))

  return (
    <Router>
      { isAuth && <NavBar /> }
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/request" element={<Request />} />
        <Route path="/requestById" element={<RequestById />} />
        <Route path="/userRegistration" element={<UserRegistration />} />
        <Route path="/entityRegistration" element={<EntityRegistration />} />
        <Route path="/requestsRegistration" element={<RequestsRegistration />} />
        <Route path="/requestsRegistration/:id" element={<RequestsRegistration />} />
        <Route path="/apresentation" element={<Apresentation />} />
      </Routes>
    </Router>
  );
}



export default App;

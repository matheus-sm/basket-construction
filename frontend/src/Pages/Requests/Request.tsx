import api from '../../Services/api'

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';

import { useState, useEffect } from 'react';

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import "./Styles/Request.css";

interface Irequest {
  id: any,
  name: any,
  id_entity: any,
  reason: string,
  basket_quantity: string
}

function Teste() {
  const [data, setData] = useState<Irequest[]>([])

  useEffect(() => {
    loadDataTable()
  }, [])

  async function loadDataTable() {
    const response = await api.get('/api/request/listAllRequests')
    setData(response.data)
  }

  const handleClose = () => setShow(false);
  
  const [show, setShow] = useState(false);

  async function handleShow({id_entity}: Irequest){
    const teste = await api.delete(`/api/entity/${id_entity}`)
    setShow(true)
  }

  

  return (
    <>
      <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}  >
        <Modal.Header closeButton>
          <Modal.Title>Dados do solicitante</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body">
          <Modal.Title className="molda-title">As informações a baixo são fornecidas pelos requisitantes</Modal.Title>

          <div className="panel-scrolling">
                  <Table striped bordered hover className="  table-custom-size-smallx">
                    <thead>
                      <tr>
                        <th className="mg-sort-columnx"  >
                          Código da requisição
                        </th>
                        <th className="mg-sort-columnx" >
                          Nome da entidade
                        </th>
                        <th className="mg-sort-columnx" >
                          Motivo
                        </th>
                        <th className="mg-sort-columnx" >
                          Quantidade
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((item: Irequest, index: number) => (
                        <tr key={index} >
                          <td onClick={() => handleShow(item.id_entity)} >{item.id}</td>
                          <td >{item.name}</td>
                          <td >{item.reason}</td>
                          <td >{item.basket_quantity}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
           
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary">Understood</Button>
        </Modal.Footer>
      </Modal>
      <section className="content">
        <div className="box">
          <div className="box-body">
            <Row >
              <Col md={12} xs={12}>
                <div className="panel-scrolling">
                  <Table striped bordered hover className="  table-custom-size-smallx">
                    <thead>
                      <tr>
                        <th className="mg-sort-columnx"  >
                          Código da requisição
                        </th>
                        <th className="mg-sort-columnx" >
                          Nome da entidade
                        </th>
                        <th className="mg-sort-columnx" >
                          Motivo
                        </th>
                        <th className="mg-sort-columnx" >
                          Quantidade
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((item: Irequest, index: number) => (
                        <tr key={index} >
                          <td onClick={() => handleShow(item.id_entity)} >{item.id}</td>
                          <td >{item.name}</td>
                          <td >{item.reason}</td>
                          <td >{item.basket_quantity}</td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              </Col>
            </Row>
          </div>
          <div className="box-footer text-right"> </div>
        </div>
      </section>
    </>
  );
}

export default Teste;
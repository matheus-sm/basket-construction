import Container from 'react-bootstrap/Container';
import { Button, Form } from 'react-bootstrap';

import React, {  useState } from 'react';

import api from '../../Services/api'

import "./Styles/Login.css"

function Login() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    try {
      event.preventDefault();

      const data = {
        name: name,
        password: password
      }

      const result = await api.post('/api/login', { data });

      localStorage.setItem('entityid', result.data.result.entityid);
      localStorage.setItem('userid', result.data.result.userid);
      localStorage.setItem('token', result.data.token);

      window.location.href = '/request'
    } catch (error) {
      alert('Usuário ou senha inválidos')
    }
  }

  return (
    <div className="login-page" >
      <Form className="form" onSubmit={handleSubmit}>
        <Container className="login-form">
          <Form.Control value={name} onChange={(event) => setName(event.target.value)} className="border" type="text" placeholder="Enter usuário" />
          <Form.Control value={password} onChange={(event) => setPassword(event.target.value)} className="border" type="password" placeholder="password" />
          <Button  type="submit">Entrar</Button>
          <p className="message">Ainda não possui conta ? <a href="/userRegistration">Criar conta</a></p>
        </Container>
      </Form>
    </div>
  );
}

export default Login;
import { Request, Response } from 'express';
import EntityModel from '../model/entityModel';

const entityModel = new EntityModel()

export default {
  async insertEntity(request: Request, response: Response) {
    try {

      const { id_user, name, nickname, address, email, city } = request.body.data

      await entityModel.insertEntity({ id_user, name, nickname, address, email, city })

      response.status(201).send({ mensagem: "Entidade criada com sucesso" })
    } catch (err) {
      response.status(500).send({ erro: err })
    }
  },

  async listOneEntity(request: Request, response: Response) {
    try {
      const { id } = request.params
      
      const entity = await entityModel.getOneEntity({id})
      
      response.status(201).json(entity)
    } catch (error) {
      response.status(500).send({ error: error })
    }
  },
}
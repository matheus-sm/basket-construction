import React from "react";
import { Link} from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Container from "react-bootstrap/esm/Container";
import './Styles/Navbar.css'

function Nav() {

  function exit() {
    window.localStorage.removeItem('entityid')
    window.localStorage.removeItem('token')
    window.localStorage.removeItem('userid')

    window.location.href = '/login'
  }

  const entityid = localStorage.getItem('entityid')

  return (
    <div className='ListTable'>
      <Navbar className="nav-blue" bg="" variant="dark">
        <Container>
          <ul className="ContainerPrimary">
            <li>
              <Link className="MainPage" to="/home">Home</Link>
            </li>
            <li>
              <Link className="PageSecondary" to="/request">Requisições</Link>
            </li>
          </ul>
          <ul className="ContainerSecondary">
          { entityid == 'null' &&
            <>
              <li>
                <Link className="PageSecondary" to="/entityRegistration">Criar entidade</Link>
              </li>
            </>
          }
          { entityid != 'null' &&
            <>
              <li>
                <Link className="PageSecondary" to="/requestsRegistration">Realizar requisição</Link>
              </li>
            </>
          }
          { entityid != 'null' &&
            <>
              <li>
                <Link className="PageSecondary" to="/requestById">Minhas requisições</Link>
              </li>
            </>
          }
            <li>
              <a className="Logout" onClick={exit}>Sair</a>
            </li>
          </ul>
        </Container>
      </Navbar>
    </div >
  );
}

export default Nav;
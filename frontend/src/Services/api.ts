import axios from 'axios'

const api = axios.create({
  baseURL: 'http://localhost:3001/'
})

api.interceptors.request.use(config => {
  const token = localStorage.getItem('token')
  if (token) {
    config.headers = {
      ...config.headers,
      ['x-access-token']: token
    }
  }
  return config
})

function exception(error: any) {
  const status = error.response.status

  if (status === 401) {
    window.localStorage.removeItem('token')
    window.location.href = '/login'
  }

  return Promise.reject(error)
}

api.interceptors.response.use(response => response, exception)

export default api;

import api from '../../Services/api'

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { AxiosError } from 'axios'

import { useState, useEffect } from "react"
import { useParams, useNavigate } from 'react-router-dom'


import Form from 'react-bootstrap/Form';

import { AiOutlineInfoCircle } from 'react-icons/ai';
import Alert from 'react-bootstrap/Alert';

import "./Styles/Request.css";

export function RequestsRegistration() {
  const [reason, setReason] = useState('')
  const [basket_quantity, setBasket_quantity] = useState('')

  const params = useParams()

  const navigate = useNavigate()

  useEffect(() => {
    params.id && loadRequest()
  }, [])
  
  async function loadRequest() {
    try {
      const { data:  requests  } = await api.get(`api/request/listOneRequest/${params.id}`)

      setReason(requests[0].reason)
      setBasket_quantity(requests[0].basket_quantity)
    } catch (error) {
      console.log(error)
    }
    }

  async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    try {
      // const teste = api.get(`api/request/listOneRequest/${41}`)
      // console.log(teste)

      event.preventDefault()

      const entityid = localStorage.getItem('entityid')

      const data = {
        id_entity: entityid,
        reason: reason,
        basket_quantity: basket_quantity
      }
      
      if (params.id) {
        await api.put(`/api/request/${params.id}`, data)
        alert('Alterações realizadas com sucesso!')
      } else {
        await api.post('api/request', { data })
        alert('Requisição cadastrado com sucesso!')
      }

      

      navigate('/requestById')

    } catch (ex: unknown) {
      if (ex instanceof AxiosError) {
        alert(`ERRO: ${ex?.response?.data.mensagem}`)
      }
    }
  }

  async function handleComeback() {
    navigate('/Request')
  }

  return (
    <section className="content">
      <div className="box">
        <div className="box-body nav-tabs-custom">
          <Row>
            <Col sm={12} md={12} xs={12}>
              <Alert className=" bg-info text-info">
                <AiOutlineInfoCircle />
                <strong > OBSERVAÇÃO:</strong> Para se cadastrar preencha as informações abaixo e clique em
                salvar, os campos com asterisco ( * ) são obrigatórios.
              </Alert>
            </Col>
          </Row>
          <Form data-toggle="validator" onSubmit={handleSubmit}>
            <Row>
              <Col sm={6} md={6} xs={6}>
                <Form.Group className="form-group">
                  <Form.Label>Motivo *</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Motivo da solicitação"  value={reason} onChange={event => setReason(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={4} md={4} xs={4}>
                <Form.Group className="form-group">
                  <Form.Label>Quantidade</Form.Label>
                  <Form.Control className="border" type="Number" placeholder="Quantidade" value={basket_quantity} onChange={event => setBasket_quantity(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={12} md={12} xs={12} className="text-right">
                <Button variant="primary" type="submit" className="m-3">  { params.id ? 'Salvar' : 'Cadastrar' } </Button>
                <Button variant="light" onClick={handleComeback}> Voltar </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    </section>
  );
}

export default RequestsRegistration;
import { Request, Response } from 'express'
import UserModel from '../model/userModel'

const userModel = new UserModel()

export default {
  async listAllUser(request: Request, response: Response) {
    try {
      const users = await userModel.listAllUser()

      response.status(200).json(users)
    } catch (error) {
      response.status(500).send({ error: error })
    }
  },

  async insertUser(request: Request, response: Response) {
    try {
      const { name, password, telephone } = request.body.data

      const user = await userModel.insertUser({ name, password, telephone })

      response.status(201).send({ mensagem: "Cadastro realizado com sucesso", user: user })
    } catch (err) {
      response.status(500).send({ erro: err })
    }
  },
}

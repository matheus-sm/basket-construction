import api from '../../Services/api'

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { AxiosError } from 'axios'

import { useState, useEffect } from "react"
import { useParams, useNavigate } from 'react-router-dom'


import "./Styles/Request.css";

interface Irequest {
  id: number,
  reason: string,
  basket_quantity: string
}


function RequestLIst() {
  
  const navigate = useNavigate()
  const [data, setData] = useState<Irequest[]>([])
  const entityid = localStorage.getItem('entityid')
  
  
  
  useEffect(() => {
    loadDataTable()
  }, [])
  


  async function loadDataTable() {
    const response = await api.get(`/api/request/listById/${entityid}`)
    setData(response.data)
  }

  async function handleDelete({ id }: Irequest) {
    try {
      await api.delete(`/api/request/${id}`)
      
      loadDataTable()
    } catch (ex: unknown) {
      if (ex instanceof AxiosError) {
        alert(`ERRO: ${ex?.response?.data.mensagem}`)
      }
    }
  }
  
  function handleEdit({ id }: Irequest) {
    navigate(`/requestsRegistration/${id}`, { replace: true })
  }

  return (
    <section className="content">
      <div className="box">
        <div className="box-body">
          <Row >
            <Col md={12} xs={12}>
              <div className="panel-scrolling">
                <Table striped bordered hover className="  table-custom-size-smallx">
                  <thead>
                    <tr>
                      <th className="mg-sort-columnx"  >
                        Código da requisição
                      </th>
                      <th className="mg-sort-columnx" >
                        Motivo
                      </th>
                      <th className="mg-sort-columnx" >
                        Quantidade
                      </th>
                      <th className="mg-sort-columnx" >
                        
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((item: Irequest, index: number) => (
                      <tr key={index}>
                        <td>{item.id}</td>
                        <td>{item.reason}</td>
                        <td>{item.basket_quantity}</td>
                        <td>
                          <Button className="m-3" variant="warning" onClick={() => handleEdit(item)}>Editar</Button>
                          <Button variant="danger" onClick={() => handleDelete(item)}>Excluir</Button>
                        </td>

                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </Col>
          </Row>
        </div>
        <div className="box-footer text-right"> </div>
      </div>
    </section>
  );
}

export default RequestLIst;
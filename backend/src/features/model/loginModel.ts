import Connection from "../../database/connection"
// import { NextFunction } from "express";
const connection: Connection = Connection.getInstance()

interface IUser {
  id?: number,
  name: string
  password?: string,
  telephone?: string,
  entityId?: number,
}

export default class loginModel {

  async authName(user: IUser): Promise<Array<IUser>> {
    try {
      const sql: string = `
      select
        public.user.id as userid,
        public.user.name,
        public.user.password,
        public.user.telephone,
        public.entity.id as entityid 
      from
        public.user
      left join 
        public.entity on public.entity.id_user = public.user.id
      where
        public.user.name = $1`

      const { name } = user

      const values = [name]

      const res: { rows: Array<IUser> } = await connection.executeQuery(sql, values)

      return res.rows
    } catch (ex) {
      throw ex
    }
  }
}


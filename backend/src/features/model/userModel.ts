import Connection from "../../database/connection"

interface IUser {
  name: string,
  password: string,
  telephone: string
}

const connection: Connection = Connection.getInstance()

export default class UserModel {

  async listAllUser(): Promise<Array<IUser>> {
    try {
      const sql: string = 'select * from public.user'

      const res: { rows: Array<IUser> } = await connection.executeQuery(sql)

      return res.rows
    } catch (ex) {
      throw ex
    }
  }

  async insertUser(user: IUser) {
    try {
      const sql = `
      insert into public.user (
        name,
        password,
        telephone
      )
      values (
        $1,
        $2,
        $3
      )`

      const { name, password, telephone } = user

      const values = [name, password, telephone]

      const res = await connection.executeQuery(sql, values)

      return res.rows
    } catch (ex) {
      throw ex
    }
  }
}
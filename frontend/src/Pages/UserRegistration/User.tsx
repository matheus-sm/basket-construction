import api from '../../Services/api'

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';

import { AiOutlineInfoCircle } from 'react-icons/ai';
import Alert from 'react-bootstrap/Alert';

import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

import { AxiosError } from 'axios'

import "./Styles/User.css"

interface IUser {
	name: String;
	password: String;
	telephone: String;
}

function UserRegistration() {
  const navigate = useNavigate()
  const [name, setName] = useState('')
	const [password, setPassword] = useState('')
	const [telephone, setTelephone] = useState('')

	function exit() {
    window.localStorage.removeItem('entityid')
    window.localStorage.removeItem('token')
		window.localStorage.removeItem('userid')

    window.location.href = '/login'
  }

	async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
		try {
			event.preventDefault()

			const data: IUser = {
				name: name,
				password: password,
				telephone: telephone
			}

			await api.post('/api/user/insertUser', { data })
			alert('Usuário cadastrado com sucesso!')

      exit()
		} catch (ex: unknown) {
			if (ex instanceof AxiosError) {
				alert(`ERRO: ${ex?.response?.data.mensagem}`)
			}
		}
	}

  async function handleComeback() {
    navigate('/login')
  }
  return (
    <section className="content">
      <div className="box">
        <div className="box-body nav-tabs-custom">
          <Row>
            <Col sm={12} md={12} xs={12}>
              <Alert className=" bg-info text-info">
                <AiOutlineInfoCircle/>  
                <strong > OBSERVAÇÃO:</strong> Para se cadastrar preencha as informações abaixo e clique em
                salvar, os campos com asterisco ( * ) são obrigatórios.
              </Alert>
            </Col>
          </Row>
          <Form data-toggle="validator"  onSubmit={handleSubmit}>
            <Row>
              <Col sm={6} md={6} xs={6}>
                <Form.Group className="form-group">
                  <Form.Label>Nome de usuário *</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Usuário" onChange={event => setName(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={4} md={4} xs={4}>
                <Form.Group className="form-group">
                  <Form.Label>Senha *</Form.Label>
                  <Form.Control className="border" type="password" placeholder="Senha" onChange={event => setPassword(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={2} md={2} xs={2}>
                <Form.Group className="form-group">
                  <Form.Label>Telefone para contato</Form.Label>
                  <Form.Control className="border" type="text" placeholder="Telefone para contato" onChange={event => setTelephone(event.target.value)} />
                </Form.Group>
              </Col>
              <Col sm={12} md={12} xs={12} className="text-right">
                <Button type="submit" className="m-3 btn btn-primary btn-cadastrar text-right" > Cadastrar </Button>
                <Button variant="light" onClick={handleComeback}> Voltar </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    </section>
  );
}

export default UserRegistration;
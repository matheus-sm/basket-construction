import Connection from "../../database/connection"
import cache from 'memory-cache'

interface IRequest {
  id?: any,
  id_entity?: any,
  reason?: string,
  basket_quantity?: number
}

interface IRequestModel {
  getAll(): Promise<Array<IRequest>>
  insert(request: IRequest): Promise<IRequest>
}

export class RequestModel implements IRequestModel {

  public async getAll(): Promise<Array<IRequest>> {

    const sql: string = `
    select 
      public.request.id,
      public.request.reason,
      public.request.basket_quantity,
      public.entity.name,
      public.request.id_entity
    from 
      public.request
    left join 
      public.entity on public.request.id_entity = public.entity.id`

    try {
      const res = await Connection.getInstance().executeQuery(sql)
      return res.rows
    } catch (ex) {
      throw ex
    }
  }

  public async insert(request: IRequest): Promise<IRequest> {
    try {
      const sql = 'insert into public.request(id_entity, reason, basket_quantity) values ($1, $2, $3)'
      const { id_entity, reason, basket_quantity } = request
      const values = [id_entity, reason, basket_quantity]

      const res = await Connection.getInstance().executeQuery(sql, values)

      return res.rows[0]
    } catch (ex) {
      throw ex
    }
  }
}

class RequestModelDecorator implements RequestModel {
  private component: RequestModel

  constructor(component: RequestModel) {
    this.component = component
  }

  public async getAll(): Promise<Array<IRequest>> {
    return this.component.getAll()
  }

  public async insert(request: IRequest): Promise<IRequest> {
    return this.component.insert(request)
  }
}

export class RequestModelCacheDecorator extends RequestModelDecorator {
  public static keyCache: string = 'REQUEST_MODEL'

  constructor(requestModel: IRequestModel) {
    super(requestModel)
  }

  public async getAll(): Promise<Array<IRequest>> {
    try {
      const cached: Array<IRequest> | null = cache.get(RequestModelCacheDecorator.keyCache)

      if (cached !== null) {
        return cached
      }

      const data = await super.getAll()
      cache.put(RequestModelCacheDecorator.keyCache, data, 6000) // one minute

      return data
    } catch (ex) {
      throw ex
    }
  }

  public async insert(request: IRequest): Promise<IRequest> {
    return super.insert(request)
  }
}

export class RequestModelGetById extends RequestModelDecorator {
  constructor(requestModel: RequestModel) {
    super(requestModel)
  }

  public async getAll(): Promise<Array<IRequest>> {
    return super.getAll()
  }

  public async insert(request: IRequest): Promise<IRequest> {
    return super.insert(request)
  }

  public async getById(request: IRequest): Promise<Array<IRequest>> {

    const sql: string = `
    select 
      public.request.id,
      public.request.reason,
      public.request.basket_quantity
    from 
      public.request
    left join 
      public.entity on public.request.id_entity = public.entity.id
    where
      public.request.id_entity = $1`

    const { id_entity } = request

    const values = [id_entity]

    try {
      const res = await Connection.getInstance().executeQuery(sql, values)
      return res.rows
    } catch (ex) {
      throw ex
    }
  }

  public async deleteRequests(request: IRequest): Promise<IRequest> {
    try {
      const sql = `delete from public.request where public.request.id = $1`

      const { id } = request

      const values = [id]

      const res = await Connection.getInstance().executeQuery(sql, values);

      return res.rows;
    } catch (ex) {
      throw ex
    }
  }

  public async getOneRequest(request: IRequest): Promise<IRequest> {
    try {
      const sql = `select id, id_entity, reason, basket_quantity from public.request where id = $1`

      const { id } = request

      const values = [id]

      const res = await Connection.getInstance().executeQuery(sql, values);

      return res.rows;
    } catch (ex) {
      throw ex
    }
  }

  public async updateRequest(request: IRequest): Promise<IRequest> {
    try {
      const sql = `update public.request set reason = $1, basket_quantity = $2 where id = $3;`
      const { id, reason, basket_quantity} = request
      const values = [reason, basket_quantity, id]

      const res = await Connection.getInstance().executeQuery(sql, values);
      return res.rows;
    } catch (ex) {
      throw ex
    }
  }
}
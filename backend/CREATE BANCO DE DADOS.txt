CREATE TABLE BASCKET_CONSTRUCTION.PUBLIC.USER(
	ID SERIAL4,
	NAME VARCHAR(255),
	PASSWORD VARCHAR(255),
	TELEPHONE VARCHAR(255),
	PRIMARY KEY (ID)
);

CREATE TABLE BASCKET_CONSTRUCTION.PUBLIC.ENTITY(
	ID SERIAL4,
	ID_USER INT4,
	NAME VARCHAR(255),
	NICKNAME VARCHAR(255),
	ADDRESS VARCHAR(255),
	EMAIL VARCHAR(255),
	CITY VARCHAR(255),
	PRIMARY KEY (ID),
	FOREIGN KEY (ID_USER) REFERENCES PUBLIC.USER(ID)
);

CREATE TABLE BASCKET_CONSTRUCTION.PUBLIC.REQUEST(
	ID SERIAL4,
	ID_ENTITY INT4,
	REASON VARCHAR(255),
	BASKET_QUANTITY INT4,
	PRIMARY KEY (ID),
	FOREIGN KEY (ID_ENTITY) REFERENCES PUBLIC.ENTITY(ID)
);

COMMIT;

--PARA POPULAR O BANCO COM INFORMAÇÕES

insert into public.user (name, password, telephone) values ('matheus',  'teste', '14992323478');
insert into public.user (name, password, telephone) values ('fernando', 'teste', '14998823478');
insert into public.user (name, password, telephone) values ('luis',     'teste', '14998823423');
insert into public.user (name, password, telephone) values ('rafael',   'teste', '14976523478');
insert into public.user (name, password, telephone) values ('anderson', 'teste', '14983453478');

COMMIT;

insert into public.entity as entity (id_user, name, nickname, address, email, city)
values (1, 'Igreja Evangélica São Bento', 'Igreja', 'Rua Pereira Martins', 'igreja.bento@gmail.com', 'Marília');

insert into public.entity as entity (id_user, name, nickname, address, email, city)
values (2, 'Escola Joaquim de Abre', 'Escola', ' Rua Estunes Mendes', 'escola.joaquim@gmail.com', 'Marília');

insert into public.entity as entity (id_user, name, nickname, address, email, city)
values (3, 'Matheus Sommacal', 'Matheus', 'Rua João Calagari', 'matheus.sommacal@gmail.com', 'Marília');

insert into public.entity as entity (id_user, name, nickname, address, email, city)
values (4, 'Instituto da Criança', 'Criança', 'Rua Bento Souza', 'instituto@gmail.com', 'Marília');

insert into public.entity as entity (id_user, name, nickname, address, email, city)
values (5, 'Igreja São Bento', 'Igreja', 'Rua Sebatioão Zambom', 'igreja.saobento@gmail.com', 'Marília');

COMMIT;

insert into public.request as r (id_entity, reason, basket_quantity)
values (1, 'Solicito uma cesta básica pois toda semana um senhor está pedindo comida na rua', 1);
insert into public.request as r (id_entity, reason, basket_quantity)
values (2, 'Estou realizando essa solicitação para ajudar uma criança carente na escola', 2);
insert into public.request as r (id_entity, reason, basket_quantity)
values (3, 'Gostaria de receber essas cestas pois sou novo na cidade e não tenho emprego', 3);
insert into public.request as r (id_entity, reason, basket_quantity)
values (4, 'Gostaria de solicitar essas cestas para ajudar algumas crianças da escola', 4);
insert into public.request as r (id_entity, reason, basket_quantity)
values (5, 'Estou realizando essa solicitação para ajudar um morador de rua que sempre está perto da igreja', 5);

COMMIT;

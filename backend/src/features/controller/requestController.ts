import { Request, Response } from 'express';

import { RequestModel, RequestModelCacheDecorator, RequestModelGetById } from '../model/requestModel'

const requestModelGetById = new RequestModelGetById(new RequestModelCacheDecorator(new RequestModel()))

// const requestModel = new RequestModel()
// const requestModelCache = new RequestModelCacheDecorator(requestModel)
// const requestModelGetById = new RequestModelGetById(requestModelCache)

export default {
  async listAllRequests(request: Request, response: Response) {
    try {
      const requests = await requestModelGetById.getAll()

      response.status(201).json(requests)
    } catch (error) {
      response.status(500).send({ error: error })
    }
  },

  async listById(request: Request, response: Response) {
    try {
      const { id_entity } = request.params
      
      const requests = await requestModelGetById.getById({id_entity})

      response.status(201).json(requests)
    } catch (error) {
      response.status(500).send({ error: error })
    }
  },

  async listOneRequest(request: Request, response: Response) {
    try {
      const { id } = request.params
      const requests = await requestModelGetById.getOneRequest({id})
      
      response.status(201).json(requests)
    } catch (error) {
      response.status(500).send({ error: error })
    }
  },
  

  async insertRequests(request: Request, response: Response) {
    try {
      const { id_entity, reason, basket_quantity } = request.body.data

      await requestModelGetById.insert({ id_entity, reason, basket_quantity })

      response.status(201).send({ mensagem: "Requisição criada com sucesso" })
    } catch (error) {
      response.status(500).send({ error: error })
    }
  },

  async updateRequests(request: Request, response: Response) {
    const {
      id
    } = request.params

    const {
      reason,
      basket_quantity
    } = request.body

    try {            
      const sector = await requestModelGetById.updateRequest({id, reason, basket_quantity })

      response.status(201).json({ mensagem: "Requisição atualizada com sucesso" })
    } catch (error) {
      response.status(500).json({ mensagem: "Não foi possível atualizar o documento", erro: error })
    }
  },

  async deleteRequests(request: Request, response: Response) {
		try {
			const { id } = request.params

			const requests = await requestModelGetById.deleteRequests({ id })

			response.status(201).json({ mensagem: "Requisição apagada com sucesso", requests: requests })
		} catch (error) {
			response.status(500).json({ mensagem: "Não foi possível deletar o documento", erro: error })
		}
	},

}